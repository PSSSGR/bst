/*
Name:Sri Padala
Description: Implementation of Binary Search Tree class.
*/
#ifndef BINARY_SEARCH_TREE_HPP
#define BINARY_SEARCH_TREE_HPP

#include <stdexcept>
#include <vector>
#include <stack>
#include <queue>
using namespace std;
template <typename T>
class BST {
public:
    // Default Ctor
    BST();
    // Parameterized Ctor
    BST(T first_elem);
    // Copy Ctor
    BST(const BST<T>& item);
    // // Move Ctor
    BST(BST<T>&& item);
    // // Dtor
    ~BST();

    class preorder_iterator;
    class inorder_iterator;
    class postorder_iterator;
    // function prbegin; pr = preorder
    preorder_iterator prbegin();
    // // function prend
    preorder_iterator prend();

    inorder_iterator begin();

    inorder_iterator end();
    // // function pobegin; po = postorder
    postorder_iterator pobegin();
    // // function poend
    postorder_iterator poend();

    // // function empty; does not throw exceptions
    bool empty()const noexcept;
    // // function min; l-value, throws underflow if empty
    T min();
    // // function min; read-only, throws underflow if empty
    const T& min()const;
    // // function max; l-value, throws underflow if empty
    T max();
    // // function max; read-only, throws underflow if empty
    const T& max()const;
    // // function insert; does not throw exceptions
    void insert(const T& item);
    // // function emplace; does not throw exceptions
    template <typename ...Args> void emplace(Args&&... args);
    // // function erase; takes a type T, throws invalid_argument if empty or cannot be found
    void erase(const T& item);
    // // function clear; does not throw exceptions
    void clear() noexcept;

    // // copy assignment operator overload
    BST& operator = (const BST<T>& item);
    // // move assignment operator overload
    BST& operator = (BST<T>&& item);
private:
    class Node;
    Node* root;
    unsigned int count = 0;
    Node* inserthelper(Node* place, const T& item);
    void clearhelper(Node* pointer)noexcept;
    Node* erasehelper(Node* place, const T& item);
    Node* minnode(Node* node)//finds the min node.
    { 
        Node* current = node; 
        while (current->getLeftLink() != nullptr) 
            current = current->getLeftLink(); 

        return current; 
    } 
};
template <typename T>
class BST<T>::Node {
public:
    // Functions that you deem necessary
    Node(){}
    Node(T thedata,Node* root,Node* left,Node* right)
        :data(thedata),Parent(root),Left_link(left),Right_link(right){}
    Node* getParentLink() const{return Parent;}
    Node* getLeftLink() const{return Left_link;}
    Node* getRightLink() const{return Right_link;}
    T& getData() {return data;}

    void setData(T theData){data = theData;}
    void setParentLink(Node* pointer){Parent = pointer;}
    void setLeftLink(Node* pointer){Left_link = pointer;}
    void setRightLink(Node* pointer){Right_link = pointer;}
private:
    T data;
    Node* Parent;
    Node* Left_link;
    Node* Right_link;
    // Data that you deem necessary
    // It shall be a doubly linked list
};



// PostORDER_ITERATOR CLASS
template <typename T>
class BST<T>::postorder_iterator {
public:   
    explicit postorder_iterator(Node *post_curr){//inline constructor
        post_current = post_curr;
        if (post_current != nullptr) {  
            makepostorderwalk(post_curr,post_stk);
            post_current = post_stk.front();
            post_stk.pop();
            post_iterator_counter = post_stk.size();
        }
    }    

    T& operator*(){return(post_current->getData());}
    postorder_iterator operator++();
    postorder_iterator operator++(int);

    friend bool operator==(const typename BST<T>::postorder_iterator &lhs,  const typename BST<T>::postorder_iterator &rhs)
    {
        return lhs.post_current == rhs.post_current;
    }
    friend bool operator!=(const typename BST<T>::postorder_iterator &lhs,  const typename BST<T>::postorder_iterator &rhs)
    {
        return !(lhs == rhs);
    }

private:
    Node *post_current;
    queue<Node*> post_stk;
    unsigned int post_iterator_counter = 0;
    void makepostorderwalk(Node* subroot,queue<Node*>& post_stk);
};

// PreORDER_ITERATOR CLASS
template <typename T>
class BST<T>::preorder_iterator {
public:  
    explicit preorder_iterator(Node *pre_curr){//inline constructor
        pre_current = pre_curr;
        if (pre_current != nullptr) {  
            makepreorderwalk(pre_curr,stk);
            pre_current = stk.front();
            stk.pop();
            iterator_counter = stk.size();
        }
    }    

    T& operator*(){return(pre_current->getData());}
    preorder_iterator operator++();
    preorder_iterator operator++(int);

    friend bool operator==(const typename BST<T>::preorder_iterator &lhs,  const typename BST<T>::preorder_iterator &rhs)
    {
        return lhs.pre_current == rhs.pre_current;
    }
    friend bool operator!=(const typename BST<T>::preorder_iterator &lhs,  const typename BST<T>::preorder_iterator &rhs)
    {
        return !(lhs == rhs);
    }

private:
    Node *pre_current;
    queue<Node*> stk;
    unsigned int iterator_counter = 0;
    void makepreorderwalk(Node* subroot,queue<Node*>& stk);
};
template <typename T>
class BST<T>::inorder_iterator {
public:
    explicit inorder_iterator(Node* in_curr){//inline constructor
            _curr = in_curr;
            if (_curr != nullptr) {  
                makeinorderwalk(_curr,in_stk);
                _curr = in_stk.front();
                in_stk.pop();
                in_iterator_counter = in_stk.size();
            }
    }
    T& operator*();
    inorder_iterator& operator++();
    inorder_iterator operator++(int);
    friend bool operator==(const typename BST<T>::inorder_iterator &lhs,  const typename BST<T>::inorder_iterator &rhs)
    {
        return lhs._curr == rhs._curr;
    }
    friend bool operator!=(const typename BST<T>::inorder_iterator &lhs,  const typename BST<T>::inorder_iterator &rhs)
    {
        return !(lhs == rhs);
    }
private:
    Node *_curr;
    queue<Node*> in_stk;
    unsigned int in_iterator_counter = 0;
    void makeinorderwalk(Node* subroot,queue<Node*>& in_stk);
};
// BST FINCTOPMS
template<typename T>
BST<T>::BST()
    :root(nullptr),count(0){}

template<typename T>
BST<T>::~BST(){}

template<typename T>
BST<T>::BST(T first_elem){
    root = new Node(first_elem,nullptr,nullptr,nullptr);
    count = 1;
}
template<typename T>
BST<T>::BST(const BST<T>& item){
    root = item.root;
    count = item.count;
}
template<typename T>
BST<T>::BST(BST<T>&& item){
    root = item.root;
    count = item.count;
    item.root = nullptr;
    item.count = 0;
}
template<typename T>
BST<T>& BST<T>::operator = (const BST<T>& item){
    Node* subroot;
    unsigned int size = 0;
    subroot = item.root;
    size = item.count;
    return(BST(subroot,size));
}
template<typename T>
BST<T>& BST<T>::operator = (BST<T>&& item){
    Node* subroot;
    unsigned int size = 0;
    subroot = item.root;
    size = item.count;
    item.root = nullptr;
    item.count = 0;
    return(BST(subroot,size));
}
template<typename T>
bool BST<T>::empty()const noexcept{
    return(count == 0);
}
template<typename T>
void BST<T>::erase(const T& item){
    if(count == 0){
        throw(std::invalid_argument("invalid"));
    }else{
        root = erasehelper(root,item);
        --count;
    } 
}
template<typename T>
typename BST<T>::Node* BST<T>::erasehelper(Node* place, const T& item){//from clrs
    if (place == nullptr){
        return place;
    } 
  
    if (item < place->getData()){
        place->setLeftLink(erasehelper(place->getLeftLink(), item)); 
  
    }else if (item > place->getData()){ 
        place->setRightLink(erasehelper(place->getRightLink(), item)); 
  
    }else{ 
        if (place->getLeftLink() == nullptr) 
        { 
            Node *temp = place->getRightLink(); 
            delete place;
            return temp; 
        } 
        else if (place->getRightLink() == nullptr) 
        { 
            Node *temp = place->getLeftLink(); 
            delete place;
            return temp; 
        } 
        Node* temp = minnode(place->getRightLink()); 
        place->getData() = temp->getData(); 
        place->setRightLink(erasehelper(place->getRightLink(), temp->getData())); 
    } 
    return place; 

}


template<typename T>
T BST<T>::min(){//go to leftmost node.
    if(root == nullptr){
        throw(std::underflow_error("noting is there"));
    }else{
        Node* current = root; 
        while (current->getLeftLink() != nullptr) { 
            current = current->getLeftLink(); 
        } 
        return(current->getData()); 
    }
}

template<typename T>
const T& BST<T>::min()const{//go to leftmost node.
    if(root == nullptr){
        throw(std::underflow_error("noting is there"));
    }else{
        Node* current = root; 
        while (current->getLeftLink() != nullptr) { 
            current = current->getLeftLink(); 
        } 
        return(current->getData()); 
    }
}
template<typename T>
T BST<T>::max(){//go to right most node.
    if(root == nullptr){
        throw(std::underflow_error("noting is there"));
    }else{
        Node* current = root;
        while (current->getRightLink() != nullptr) { 
            current = current->getRightLink(); 
        } 
        return(current->getData()); 
    }

}
template<typename T>
const T& BST<T>::max()const{//go to right most node.
    if(root == nullptr){
        throw(std::underflow_error("noting is there"));
    }else{
        Node* current = root; 
        while (current->getRightLink() != nullptr) { 
            current = current->getRightLink(); 
        } 
        return(current->getData()); 
    }
}

template<typename T>
void BST<T>::insert(const T& item){
    root = inserthelper(root,item);
    count++;
    
}
template<typename T>
typename BST<T>::Node* BST<T>::inserthelper(typename BST<T>::Node* place, const T& item){
     if(place == nullptr){
        place = new Node(item,nullptr,nullptr,nullptr);
    }else if(place->getData() < item){
        Node* temp = inserthelper(place->getRightLink(),item);
        place->setRightLink(temp);
        temp->setParentLink(place);
    }else{
        Node* temp = inserthelper(place->getLeftLink(),item);
        place->setLeftLink(temp);
        temp->setParentLink(place);
    }
    return(place);

}
template<typename T>
template <typename ...Args> 
void BST<T>::emplace(Args&&... args){
    insert(std::move(T(std::forward<Args>(args) ...)));
}
template<typename T>
void BST<T>::clear() noexcept{
    clearhelper(root);
}
template<typename T>
void BST<T>::clearhelper(Node* pointer) noexcept{
    if(pointer == nullptr){
        return;
    }
    clearhelper(pointer->getLeftLink());
    clearhelper(pointer->getRightLink());
    --count;
    delete pointer;
    pointer = nullptr;
}


// PREORDER_ITERATOR FUNCTIONS
template <typename T>
typename BST<T>::preorder_iterator BST<T>::preorder_iterator::operator++(){
    if( iterator_counter > 0){
        pre_current = stk.front();
        stk.pop();
        --iterator_counter;
        return*this;
    }else{
        pre_current = nullptr;
        return*this;
    }
}
template <typename T>
typename BST<T>::preorder_iterator BST<T>::preorder_iterator::operator++(int)
{
    preorder_iterator tmp = *this;
    ++(*this);

    return tmp;
}

template <typename T>
typename BST<T>::preorder_iterator BST<T>::prbegin(){
    Node *curr = root;
    return preorder_iterator(curr);
}
template <typename T>
typename BST<T>::preorder_iterator BST<T>::prend(){
    return(preorder_iterator(nullptr));
}
template <typename T>
void  BST<T>::preorder_iterator::makepreorderwalk(Node* subroot,queue<Node*>& stk){
        if (subroot == nullptr) {
                return; 
        }
        stk.push(subroot);
        makepreorderwalk(subroot->getLeftLink(),stk);          
        makepreorderwalk(subroot->getRightLink(),stk); 
}
// POSTORDER_ITERATOR FUNCTIONS
template <typename T>
typename BST<T>::postorder_iterator BST<T>::postorder_iterator::operator++(){
    if( post_iterator_counter > 0){
        post_current = post_stk.front();
        post_stk.pop();
        --post_iterator_counter;
        return*this;
    }else{
        post_current = nullptr;
        return*this;
    }
}
template <typename T>
typename BST<T>::postorder_iterator BST<T>::postorder_iterator::operator++(int)
{
    postorder_iterator tmp = *this;
    ++(*this);
    return tmp;
}
template <typename T>
void  BST<T>::postorder_iterator::makepostorderwalk(Node* subroot,queue<Node*>& post_stk){
        if (subroot == nullptr) {
                return; 
        }
        makepostorderwalk(subroot->getLeftLink(),post_stk); 
        makepostorderwalk(subroot->getRightLink(),post_stk); 
        post_stk.push(subroot);
}
template <typename T>
void  BST<T>::inorder_iterator::makeinorderwalk(Node* subroot,queue<Node*>& in_stk){
        if (subroot == nullptr) {
                return; 
        }
        makeinorderwalk(subroot->getLeftLink(),in_stk);
        in_stk.push(subroot); 
        makeinorderwalk(subroot->getRightLink(),in_stk); 
}
template <typename T>
typename BST<T>::postorder_iterator BST<T>::pobegin(){
      Node *curr = root;
  
        if (curr != nullptr){
                while (curr->getLeftLink() != nullptr)
                    curr = curr->getLeftLink();
        } 
        return(postorder_iterator(curr));


}
template <typename T>
typename BST<T>::postorder_iterator BST<T>::poend(){
    return postorder_iterator(nullptr);   
}
// INORDER_ITERATOR FUNCTIONS
template <typename T>
typename BST<T>::inorder_iterator BST<T>::begin()//gets the first element.
{
  Node *curr = root;
  
  if (curr != nullptr){
    while (curr->getLeftLink() != nullptr)
      curr = curr->getLeftLink();
    }
    return inorder_iterator(curr);
}
template <typename T>
typename BST<T>::inorder_iterator BST<T>::end()//iterator to nullptr
{
    return inorder_iterator(nullptr);
}
template <typename T>
T& BST<T>::inorder_iterator::operator*()
{
    return _curr->getData();
}

template <typename T>
typename BST<T>::inorder_iterator& BST<T>::inorder_iterator::operator++()
{
    if( in_iterator_counter > 0){
        _curr = in_stk.front();
        in_stk.pop();
        --in_iterator_counter;
        return*this;
    }else{
        _curr = nullptr;
        return*this;
    }
}

template <typename T>
typename BST<T>::inorder_iterator BST<T>::inorder_iterator::operator++(int)
{
    inorder_iterator tmp = *this;
    ++(*this);

    return tmp;
}

#endif